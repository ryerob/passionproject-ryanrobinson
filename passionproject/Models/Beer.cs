﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace passionproject.Models
{
    public class Beer
    {
        [Key]
        public int BeerID { get; set; }

        [Required, StringLength(255), Display(Name = "Beer Name")]
        public string BeerName { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Beer Description")]
        public string BeerDescription { get; set; }

        public virtual ICollection<Review> Review { get; set; }
    }
}