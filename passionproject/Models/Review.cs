﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace passionproject.Models
{
    public class Review
    {
        [Key]
        public int ReviewId { get; set; }

        [Required, StringLength(80), Display(Name = "Title")]
        public string Title { get; set; }

        [Required, StringLength(30), Display(Name = "Name")]
        public string UserName { get; set; }

        [Required, StringLength(255), Display(Name = "Review")]
        public string ReviewBody { get; set; }

        public virtual Beer beer
        { get; set; }
    }
}